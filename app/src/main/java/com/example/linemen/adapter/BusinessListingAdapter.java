package com.example.linemen.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.linemen.R;

public class BusinessListingAdapter extends RecyclerView.Adapter<BusinessListingAdapter.MyViewHodler>{

    private Context context;


    public BusinessListingAdapter(Context context ) {
        this.context = context;

    }


    @NonNull
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_listings_adapter,viewGroup,false);

        return new BusinessListingAdapter.MyViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHodler myViewHodler, int i) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHodler extends RecyclerView.ViewHolder {
        private TextView tvOrderPrice;
        public MyViewHodler(@NonNull View itemView) {
            super(itemView);

        }
    }

}
