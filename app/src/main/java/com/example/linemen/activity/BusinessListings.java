package com.example.linemen.activity;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.linemen.R;
import com.example.linemen.adapter.BusinessListingAdapter;
import com.example.linemen.utility.BaseAppCompatActivity;

import butterknife.BindView;

public class BusinessListings extends BaseAppCompatActivity {

    @BindView(R.id.list_businessListing)
    RecyclerView recyclerOfferList;

    BusinessListingAdapter adapter;

    @Override
    protected int layoutResourceId() {
        return R.layout.business_listings;
    }

    @Override
    protected void onActivityLaunched() {

        recyclerOfferList.setLayoutManager(new GridLayoutManager(this, 1,
                GridLayoutManager.VERTICAL, false));
        recyclerOfferList.setItemAnimator(new DefaultItemAnimator());
        recyclerOfferList.setNestedScrollingEnabled(false);

        adapter=new BusinessListingAdapter(this);
        recyclerOfferList.setAdapter(adapter);
    }
}
