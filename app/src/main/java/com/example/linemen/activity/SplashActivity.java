package com.example.linemen.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.linemen.R;

public class SplashActivity extends AppCompatActivity {

    private Handler splashHandler;
    private Runnable splashRunnable;
    private static final int SPLASH_DELAY_SHORT = 3000;
    boolean goToLogin=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_view_pager);
        setFullScreen();

        splashHandler = new Handler(getMainLooper());
        splashRunnable = new Runnable() {
            @Override
            public void run() {
/*
                try{
                    goToLogin=SharedPreferenceManager.getInstance().getBooleanFromPreferences("goToLogin");
                }catch (Exception e){
                    goToLogin=false;
                    e.printStackTrace();
                }*/

              //  if (goToLogin){
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                  //  launchActivity(MainActivity.class, true);
//                }else {
//                    launchActivity(LoginActivity.class, true);
//                }
            }
        };
        splashHandler.postDelayed(splashRunnable, SPLASH_DELAY_SHORT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (splashHandler != null && splashRunnable != null) {
            splashHandler.removeCallbacks(splashRunnable);
        }
    }

    private void setFullScreen() {
        final View decorView = getWindow().getDecorView();
        final int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        decorView.setSystemUiVisibility(uiOptions);
    }
}
